// CS3650 CH02 starter code
// Fall 2017
//
// Author: Nat Tuck
// Once you've read this, you're done
// with HW07.


#include <stdint.h>
#include <sys/mman.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include "xmalloc.h"
#include <pthread.h>
typedef struct nu_free_cell {
    int64_t              size;
    struct nu_free_cell* next;
} nu_free_cell;

static const int64_t CHUNK_SIZE = 4096;
static const int64_t CELL_SIZE  = (int64_t)sizeof(nu_free_cell);


static size_t binsize[10] = {1000000,8192, 4096, 2048, 1024, 512, 256, 128, 64, 32};
 __thread nu_free_cell* bins[10];

void 
get_more_pages() {

	void* pages = mmap(NULL, CHUNK_SIZE * 900, PROT_READ|PROT_WRITE, MAP_ANONYMOUS|MAP_PRIVATE, -1, 0);
	
	nu_free_cell* temps= pages;
	temps->size = CHUNK_SIZE * 300;
	temps->next = 0;

	pages += CHUNK_SIZE * 300;
	bins[0] = temps;
	
	for (size_t i = 0; i < 6400; i++) {
		nu_free_cell* temp= pages;
		temp->size = 32;
		temp->next = bins[9];
		bins[9] = temp;
		pages += 32;
	}

	for (size_t i = 0; i < 200; i++) {
		nu_free_cell* temp = pages;
		temp->size = CHUNK_SIZE / 2;
		temp->next = bins[3];
		bins[3] = temp;
		pages += CHUNK_SIZE / 2;

	}



	for (size_t i = 0; i < 225; i++) {
		nu_free_cell* temp = pages;
		temp->size = CHUNK_SIZE * 2;
		temp->next = bins[1];
		bins[1] = temp;
		pages += CHUNK_SIZE * 2;

	}
}


long ilog2(long x) {

	long y = 0;
	long copy = x;
	while (x > 0) {
		x = x >> 1;
		y++;
		
	}

	return y;
}



int getbinnum(size_t size) {

	int binNum = ilog2(size);
	if (binNum < 5) {
		binNum = 5;
	}
	if (binNum > 13) {
		binNum = 14;
	
	}

	return binNum;

}


static
nu_free_cell*
free_list_get_cell(int64_t size)
{
	int binNum = ilog2(size);	
	return bins[binNum - 5];
}



void cascadedownto(int currentbin, int bindownto) {

	if(currentbin == bindownto && currentbin > -1) {
		return;
	}

    else{
		nu_free_cell* node = bins[currentbin];
		bins[currentbin] = node -> next;
		node ->next = 0;
		
		size_t splitsize = node->size /2;
		nu_free_cell* split1 = node;
		nu_free_cell* split2 = ((void*) node) + splitsize;
		split1->size = splitsize;
		split2->size = splitsize;
		split1->next = split2;
		split2->next = bins[currentbin + 1];
		bins[currentbin + 1] = split1;
		cascadedownto(currentbin + 1, bindownto);
    
    }   
}

void coalesce(nu_free_cell* current) {


    nu_free_cell* next = current->next;

    if((void*)current + current->size == (void*) next) {
            current->size = current->size + next->size;
            current->next = next->next;
        }


}
void splitdown(int currentbin, int bindownto, size_t size) {

    if (currentbin ==  0) {
		get_more_pages();
		cascadedownto(currentbin + 1, bindownto);
    }

    else if ((void*)bins[currentbin] == 0 || size > CHUNK_SIZE * 2) {
        splitdown(currentbin - 1, bindownto, size);

    }

    else {
        cascadedownto(currentbin, bindownto);
    }

}




void
remove_node(nu_free_cell* head, nu_free_cell* toRemove) {


	nu_free_cell* current  = head;
	
	if (toRemove == head) {
		head = head->next;
		return;
	}
	
	nu_free_cell* previous = head;
	while(current != 0) {
		if((void*)toRemove == (void*)current) {
			previous->next = current->next;
			return;
		}
		previous = current;
		current = current->next;
	}



}

void*
emptySpace(nu_free_cell* head, size_t size) {

	nu_free_cell* current = head;
	if((void*)current == 0) {
		return 0;
	}

	else {
		while((void*)current!= 0) {	
			size_t free_size = current->size;
			if(free_size  > size) {
				remove_node(head, current);
				return (void*) current;
			}
			current = current->next;
		}
	}

	return 0;

}
void 
insert(size_t size , void* addr, nu_free_cell* head) {

	if(head == 0 || ((void*) head) > addr ) {
		nu_free_cell* node = addr;
			
		node->size = size;
		node->next = head;
		head = node;

	}

	else {
		nu_free_cell* current = head;
		while(current->next != 0 && addr > ((void*) current->next)) {
			current = current->next;
		}
		nu_free_cell* node = addr;
		node->size = size;
		node->next = 0;
		node->next = current->next;
		current->next = node;
		coalesce(node); // coalesce with cell after newly inserted
		coalesce(current); // coalesce with cell before newly inserted
	}
}



void*
xmalloc(size_t usize)
{
    int64_t size = (int64_t) usize;
    int64_t alloc_size = size + sizeof(int64_t);

	if ((void*)(bins[1]) == 0) {
		get_more_pages();
	}
	
	nu_free_cell* cell = 0;
	if(alloc_size > CHUNK_SIZE * 2) {
		cell = (nu_free_cell*)emptySpace(bins[0], alloc_size);
		// when a chunk is needed than the largest chunk in the free_list
		if(cell == 0) {
			cell = (nu_free_cell*) mmap(NULL, alloc_size, PROT_READ|PROT_WRITE, MAP_ANONYMOUS|MAP_PRIVATE, -1, 0);
		}
	}
	
	else {	
		int binNum = getbinnum(alloc_size);
		// larger bins have earlier array indexs. The smallest power of 2 bin offered is 32 bytes
		binNum = 9 - (binNum - 5);
		// edge case where alloc_size is a power of 2
		if (binsize[binNum  + 1] == alloc_size) {
			binNum++;
		}	
		
		cell = bins[binNum];
		usize = binsize[binNum] - sizeof(int64_t);
		
		if (!cell && binNum != 0) {
			splitdown(binNum - 1, binNum, alloc_size);
			cell = bins[binNum];
		}

		bins[binNum] = cell->next;
		
	}
	*((int64_t*)cell) = usize;
    return ((void*)cell) + sizeof(int64_t);
}

void
xfree(void* addr) 
{
    nu_free_cell* cell = (nu_free_cell*)(addr - sizeof(int64_t));
    int64_t size = *((int64_t*) cell) + sizeof(int64_t);


	if( size > CHUNK_SIZE * 8192) {
		insert(size, cell, bins[0]);
		return;
	}

	else { 
	
	int binNum = getbinnum(size);

	binNum = 9 - (binNum  - 5);

	// if power of 2
	if (binsize[binNum + 1] == size) {
		binNum++;
	}

	cell->size = size;
	cell->next = bins[binNum];
	bins[binNum] = cell;
	}
}



void*
xrealloc(void *prev, size_t bytes) {
    prev = prev - sizeof(size_t);
	int64_t prevsize = *((int64_t*)prev);
	prev = prev + sizeof(size_t);
	

	if(bytes == 0) {
		xfree(prev);
		return 0;
	}

	if (prev == 0) {
		void* chunk = xmalloc(bytes);
		return chunk;
	}
	void* realloc = xmalloc(bytes);
    
	if (bytes<= prevsize) {
		memcpy(realloc, prev,bytes);
	}

	else {
		memcpy(realloc, prev, prevsize);
	}
	xfree(prev);
    return realloc;
}
