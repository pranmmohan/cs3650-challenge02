
#include <stdlib.h>
#include <sys/mman.h>
#include <stdio.h>
#include <math.h>
#include "xmalloc.h"
#include <string.h>
#include <pthread.h>

/*
  typedef struct hm_stats {
  long pages_mapped;
  long pages_unmapped;
  long chunks_allocated;
  long chunks_freed;
  long free_length;
  } hm_stats;
*/

typedef struct free_list {
	size_t size;
	struct free_list* next;
} free_list;

typedef struct header {
	size_t size;
} header;

static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
static free_list* head = 0; // this initializes the pointer to NULL
const size_t PAGE_SIZE = 4096;
static size_t size_available = 0;



long
free_list_length()
{

	long i = 0;
	free_list* current = head;
	while(current!= 0) {
		current = current->next;
		i++;
	}
    return i;
}



static
size_t
div_up(size_t xx, size_t yy)
{
    // This is useful to calculate # of pages
    // for large allocations.
    size_t zz = xx / yy;

    if (zz * yy == xx) {
        return zz;
    }
    else {
        return zz + 1;
    }
}

void coalesce(free_list* current) {


	free_list* next = current->next;

	if((void*)current + current->size == (void*) next) {
			current->size = current->size + next->size;
			current->next = next->next;
		}


}




void 
xinsert(size_t size , void* addr) {
	pthread_mutex_lock(&lock);
	if(head == 0 || ((void*) head) > addr ) {
		free_list* node = addr;
			
		node->size = size;
		node->next = head;
		head = node;

	}

	else {
		free_list* current = head;
		while(current->next != 0 && addr > ((void*) current->next)) {
			current = current->next;
		}
		free_list* node = addr;
		node->size = size;
		node->next = 0;
		node->next = current->next;
		current->next = node;
		coalesce(node); // coalesce with cell after newly inserted
		coalesce(current); // coalesce with cell before newly inserted
	}
	pthread_mutex_unlock(&lock);
}
void
remove_node(free_list* toRemove) {


	free_list* current  = head;
	
	if (toRemove == head) {
		head = head->next;
		return;
	}
	
	free_list* previous = head;
	while(current != 0) {
		if((void*)toRemove == (void*)current) {
			previous->next = current->next;
			return;
		}
		previous = current;
		current = current->next;
	}



}

void*
emptySpace(size_t size) {

	pthread_mutex_lock(&lock);
	free_list* current = head;
	if((void*)current == 0) {
		pthread_mutex_unlock(&lock);
		return 0;
	}

	else {
		while((void*)current!= 0) {	
			size_t free_size = current->size;
			if(free_size  > size) {
				remove_node(current);
				pthread_mutex_unlock(&lock);
				return (void*) current;
			}
			current = current->next;
		}
	}
	pthread_mutex_unlock(&lock);
	return 0;

}






void*
xmalloc(size_t size)
{
	size += sizeof(size_t);
	void*  return_addr = 0;
	
	if (size > PAGE_SIZE) {
		size_t pages_needed = div_up(size, PAGE_SIZE);
		size = PAGE_SIZE * pages_needed;
		return_addr = mmap(NULL, PAGE_SIZE * pages_needed, PROT_READ|PROT_WRITE, MAP_ANONYMOUS|MAP_PRIVATE, -1, 0);
		((header*)return_addr) ->size = size - sizeof(size_t);
	}

	else  {


		void* location = emptySpace(size);
		if (location != 0) {
			return_addr = location;
			size_t return_size = ((header*)return_addr) ->size;
			if (return_size - size > sizeof(free_list)) {
				xinsert(return_size - size, return_addr + size);
			}
			((header*)return_addr) ->size = size - sizeof(size_t);
		}

		else {
			return_addr = mmap(NULL, PAGE_SIZE, PROT_READ|PROT_WRITE, MAP_ANONYMOUS|MAP_PRIVATE, -1, 0);
			if (PAGE_SIZE - size >  sizeof(free_list)) {
				xinsert(PAGE_SIZE - size, (void*)(return_addr) + size);			
			}
			((header*)return_addr) ->size = size - sizeof(size_t);
		}
	}

	return return_addr + sizeof(size_t);
}

void
xfree(void* item)
{

	item = item - sizeof(size_t);
	size_t blocksize= ((header*)item)->size;	
    if (blocksize > PAGE_SIZE) {
		size_t numblocks = div_up(blocksize, PAGE_SIZE);
		munmap(item, PAGE_SIZE*numblocks);
	}

	else {
		xinsert(blocksize + sizeof(size_t), item);
	}
}


void*
xrealloc(void *prev, size_t bytes) {
	void* realloc = xmalloc(bytes);
	prev = prev - sizeof(size_t);
	size_t prevsize = ((header*)prev)->size;
	memcpy(realloc, prev, prevsize);
	return realloc;
}
